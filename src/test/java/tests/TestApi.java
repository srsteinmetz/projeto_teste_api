package tests;


import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;

public class TestApi {
	
	String url = "https://jsonplaceholder.typicode.com";
		
	@Before
	public void setup() {
		RestAssured.baseURI = url ;
	}
		
	@Test
	public void chamadaGET() {
		given().when().get(url+"/posts").then().statusCode(200);				
	}
	
	@Test
	public void chamadaGET_1() {
		given().when().get(url+"/posts/1").then().statusCode(200);				
	}
	
	@Test
	public void chamadaGET_comments() {
		given().when().get(url+"/posts/1/comments").then().statusCode(200);				
	}
	
	@Test
	public void chamadaGET_postId() {
		given().when().get(url+"/comments?postId=1").then().statusCode(200);				
	}
	
	@Test
	public void chamadaGET_userId() {
		given().when().get(url+"/posts?userId=1").then().statusCode(200);				
	}
	
	@Test
	public void chamadaPOST() {
		given().when().post(url+"/posts").then().statusCode(201);
	}
	
	@Test
	public void chamadaPUT() {
		given().when().put(url+"/posts/1").then().statusCode(200);
	}
	
	@Test
	public void chamadaDELETE() {
		given().when().delete(url+"/posts/1").then().statusCode(200);
	}
	
	@Test
	public void chamadaPATCH() {
		given().when().patch(url+"/posts/1").then().statusCode(200);
	}
	
	@Test
	public void chamadaGET_exception() {
		given().param("userId", "0").when().get(url+"/posts?userId=").then().statusCode(404);
													   				
	}
	
	@Test
	public void chamadaDELETE_exception() {
		given().param("userId", "0").when().delete(url+"/posts").then().statusCode(404);
													   				
	}
	
	

}
